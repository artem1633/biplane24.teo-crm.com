<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mobile_user`.
 */
class m191101_162048_create_mobile_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('mobile_user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->comment('Логин'),
            'token' => $this->string()->comment('Токен'),
            'push_token' => $this->string()->comment('Токен для пушей'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'rate' => $this->string()->comment('Тариф'),
            'rate_end_datetime' => $this->dateTime()->comment('Дата и время окончания тарифа'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('mobile_user');
    }
}
