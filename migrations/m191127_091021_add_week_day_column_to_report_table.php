<?php

use yii\db\Migration;

/**
 * Handles adding week_day to table `report`.
 */
class m191127_091021_add_week_day_column_to_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report', 'week_day', $this->integer()->comment('День недели'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report', 'week_day');
    }
}
