<?php

use yii\db\Migration;

/**
 * Handles the creation of table `share_data`.
 */
class m191101_172450_create_share_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('share_data', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'href' => $this->string()->comment('Ссылка'),
            'time_from' => $this->dateTime()->comment('Время с'),
            'time_to' => $this->dateTime()->comment('Время по'),
            'comment' => $this->text()->comment('Комментарий'),
        ]);

        $this->createIndex(
            'idx-share_data-user_id',
            'share_data',
            'user_id'
        );

        $this->addForeignKey(
            'fk-share_data-user_id',
            'share_data',
            'user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-share_data-user_id',
            'share_data'
        );

        $this->dropIndex(
            'idx-share_data-user_id',
            'share_data'
        );

        $this->dropTable('share_data');
    }
}
