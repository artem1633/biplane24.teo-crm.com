<?php

use yii\db\Migration;

/**
 * Handles the creation of table `location`.
 */
class m191220_160720_create_location_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('location', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'app_id' => $this->string()->comment('ID приложения'),
            'coords' => $this->string()->comment('Координаты'),
            'value' => $this->integer()->comment('Значение'),
            'place_id' => $this->integer()->comment('Место'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
            'time_start' => $this->integer()->comment('Время начала'),
            'time_work' => $this->integer()->comment('Время работы'),
            'week_day' => $this->integer()->comment('День недели'),
        ]);

        $this->createIndex(
            'idx-location-user_id',
            'location',
            'user_id'
        );

        $this->addForeignKey(
            'fk-location-user_id',
            'location',
            'user_id',
            'mobile_user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-location-place_id',
            'location',
            'place_id'
        );

        $this->addForeignKey(
            'fk-location-place_id',
            'location',
            'place_id',
            'place',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-location-user_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-user_id',
            'location'
        );

        $this->dropForeignKey(
            'fk-location-place_id',
            'location'
        );

        $this->dropIndex(
            'idx-location-place_id',
            'location'
        );

        $this->dropTable('location');
    }
}
