<?php

use yii\db\Migration;

/**
 * Class m191110_124814_alter_time_from_column_and_time_to_column_from_share_data_table
 */
class m191110_124814_alter_time_from_column_and_time_to_column_from_share_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('share_data', 'time_from', $this->string()->comment('Время с'));
        $this->alterColumn('share_data', 'time_to', $this->string()->comment('Время по'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('share_data', 'time_from', $this->dateTime()->comment('Время с'));
        $this->alterColumn('share_data', 'time_to', $this->dateTime()->comment('Время по'));
    }
}
