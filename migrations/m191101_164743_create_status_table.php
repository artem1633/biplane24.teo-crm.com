<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m191101_164743_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Польвазователь'),
            'name' => $this->string()->comment('Наименование'),
        ]);

        $this->createIndex(
            'idx-status-user_id',
            'status',
            'user_id'
        );

        $this->addForeignKey(
            'fk-status-user_id',
            'status',
            'user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-status-user_id',
            'status'
        );

        $this->dropIndex(
            'idx-status-user_id',
            'status'
        );

        $this->dropTable('status');
    }
}
