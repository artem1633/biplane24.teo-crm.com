<?php

use yii\db\Migration;

/**
 * Handles the creation of table `group`.
 */
class m191101_164750_create_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Польвазователь'),
            'name' => $this->string()->comment('Наименование'),
        ]);

        $this->createIndex(
            'idx-group-user_id',
            'group',
            'user_id'
        );

        $this->addForeignKey(
            'fk-group-user_id',
            'group',
            'user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-group-user_id',
            'group'
        );

        $this->dropIndex(
            'idx-group-user_id',
            'group'
        );
        
        $this->dropTable('group');
    }
}
