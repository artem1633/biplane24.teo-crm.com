<?php

use yii\db\Migration;

/**
 * Handles the creation of table `round`.
 */
class m191101_173017_create_round_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('round', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'href_user_id' => $this->integer()->comment('Пользователь на кого ссылаемся'),
            'href' => $this->string()->comment('Ссылка'),
            'name' => $this->string()->comment('ФИО'),
            'status_id' => $this->integer()->comment('Статус должность'),
            'group_id' => $this->integer()->comment('Группа'),
        ]);

        $this->createIndex(
            'idx-round-user_id',
            'round',
            'user_id'
        );

        $this->addForeignKey(
            'fk-round-user_id',
            'round',
            'user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-round-href_user_id',
            'round',
            'href_user_id'
        );

        $this->addForeignKey(
            'fk-round-href_user_id',
            'round',
            'href_user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-round-group_id',
            'round',
            'group_id'
        );

        $this->addForeignKey(
            'fk-round-group_id',
            'round',
            'group_id',
            'group',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-round-status_id',
            'round',
            'status_id'
        );

        $this->addForeignKey(
            'fk-round-status_id',
            'round',
            'status_id',
            'status',
            'id',
            'SET NULL'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-round-user_id',
            'round'
        );

        $this->dropIndex(
            'idx-round-user_id',
            'round'
        );

        $this->dropForeignKey(
            'fk-round-href_user_id',
            'round'
        );

        $this->dropIndex(
            'idx-round-href_user_id',
            'round'
        );

        $this->dropForeignKey(
            'fk-round-group_id',
            'round'
        );

        $this->dropIndex(
            'idx-round-group_id',
            'round'
        );

        $this->dropForeignKey(
            'fk-round-status_id',
            'round'
        );

        $this->dropIndex(
            'idx-round-status_id',
            'round'
        );

        $this->dropTable('round');
    }
}
