<?php

use yii\db\Migration;

/**
 * Handles the creation of table `report`.
 */
class m191101_174222_create_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('report', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'app_id' => $this->string()->comment('ID приложения'),
            'coords' => $this->string()->comment('Координаты'),
            'value' => $this->integer()->comment('Значение'),
            'place_id' => $this->integer()->comment('Место'),
            'datetime' => $this->dateTime()->comment('Дата и время'),
        ]);

        $this->createIndex(
            'idx-report-user_id',
            'report',
            'user_id'
        );

        $this->addForeignKey(
            'fk-report-user_id',
            'report',
            'user_id',
            'mobile_user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-report-place_id',
            'report',
            'place_id'
        );

        $this->addForeignKey(
            'fk-report-place_id',
            'report',
            'place_id',
            'place',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-report-user_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-user_id',
            'report'
        );

        $this->dropForeignKey(
            'fk-report-place_id',
            'report'
        );

        $this->dropIndex(
            'idx-report-place_id',
            'report'
        );

        $this->dropTable('report');
    }
}
