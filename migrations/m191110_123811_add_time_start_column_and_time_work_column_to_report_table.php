<?php

use yii\db\Migration;

/**
 * Handles adding time_start_column_and_time_work to table `report`.
 */
class m191110_123811_add_time_start_column_and_time_work_column_to_report_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('report', 'time_start', $this->integer()->comment('Время начала'));
        $this->addColumn('report', 'time_work', $this->integer()->comment('Время работы'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('report', 'time_start');
        $this->dropColumn('report', 'time_work');
    }
}
