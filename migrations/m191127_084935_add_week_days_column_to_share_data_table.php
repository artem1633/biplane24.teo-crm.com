<?php

use yii\db\Migration;

/**
 * Handles adding week_days to table `share_data`.
 */
class m191127_084935_add_week_days_column_to_share_data_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('share_data', 'week_days', $this->string()->comment('Дни недели'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('share_data', 'week_days');
    }
}
