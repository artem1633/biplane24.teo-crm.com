<?php

use yii\db\Migration;

/**
 * Handles the creation of table `place`.
 */
class m191101_173728_create_place_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('place', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'name' => $this->string()->comment('Название'),
            'position1' => $this->string()->comment('Позиция1'),
            'position2' => $this->string()->comment('Позиция2'),
            'position3' => $this->string()->comment('Позиция3'),
            'position4' => $this->string()->comment('Позиция4'),
            'radius' => $this->float()->comment('Радиус'),
        ]);

        $this->createIndex(
            'idx-place-user_id',
            'place',
            'user_id'
        );

        $this->addForeignKey(
            'fk-place-user_id',
            'place',
            'user_id',
            'mobile_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-place-user_id',
            'place'
        );

        $this->dropIndex(
            'idx-place-user_id',
            'place'
        );

        $this->dropTable('place');
    }
}
