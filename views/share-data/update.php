<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ShareData */
?>
<div class="share-data-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
