<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ShareData */
?>
<div class="share-data-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'href',
            'datetime_from',
            'datetime_to',
            'comment:ntext',
        ],
    ]) ?>

</div>
