<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MobileUser */
?>
<div class="mobile-user-view">

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'login',
                    'token',
                    'push_token',
                    'last_activity_datetime',
                    'rate',
                    'rate_end_datetime',
                    'created_at',
                ],
            ]) ?>
        </div>
    </div>

</div>
