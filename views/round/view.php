<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Round */
?>
<div class="round-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'href_user_id',
            'href',
            'name',
            'status_id',
            'group_id',
        ],
    ]) ?>

</div>
