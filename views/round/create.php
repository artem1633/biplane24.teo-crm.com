<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Round */

?>
<div class="round-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
