<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
?>
<div class="report-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'app_id',
            'coords',
            'value',
            'place_id',
            'datetime',
        ],
    ]) ?>

</div>
