<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
?>
<div class="report-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
