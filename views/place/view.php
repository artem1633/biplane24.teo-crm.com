<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Place */
?>
<div class="place-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'name',
            'position1',
            'position2',
            'position3',
            'position4',
            'radius',
        ],
    ]) ?>

</div>
