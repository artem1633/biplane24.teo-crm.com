<?php

use yii\helpers\Url;

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        echo \app\admintheme\widgets\TopMenu::widget(
            [
                'options' => ['class' => 'nav'],
                'items' => [
                    ['label' => 'Пользователи', 'icon' => 'fa  fa-user-o', 'url' => ['/user'],],
                    ['label' => 'Мобильные пользователи', 'icon' => 'fa  fa-users', 'url' => ['/mobile-user'],],
                    ['label' => 'Места', 'icon' => 'fa  fa-map-marker', 'url' => ['/place'],],
                    ['label' => 'Окружения', 'icon' => 'fa  fa-globe', 'url' => ['/round'],],
                    ['label' => 'Данные', 'icon' => 'fa  fa-th', 'url' => ['/share-data'],],
                    ['label' => 'Отчет', 'icon' => 'fa  fa-bar-chart-o', 'url' => ['/report'],],
                    ['label' => 'Настройки', 'icon' => 'fa  fa-cog', 'url' => ['/settings'],],
                ],
            ]
        );
        ?>
    <?php endif; ?>
</div>
