<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mobile_user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $token Токен
 * @property string $push_token Токен для пушей
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $rate Тариф
 * @property string $rate_end_datetime Дата и время окончания тарифа
 * @property string $created_at
 *
 * @property Group[] $groups
 * @property Place[] $places
 * @property Report[] $reports
 * @property Round[] $rounds
 * @property Round[] $rounds0
 * @property ShareData[] $shareDatas
 * @property Status[] $statuses
 */
class MobileUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_activity_datetime', 'rate_end_datetime', 'created_at'], 'safe'],
            [['login', 'token', 'push_token', 'rate'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'token' => 'Токен',
            'push_token' => 'Токен для пушей',
            'last_activity_datetime' => 'Дата и время последней активности',
            'rate' => 'Тариф',
            'rate_end_datetime' => 'Дата и время окончания тарифа',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->token = Yii::$app->security->generateRandomString();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaces()
    {
        return $this->hasMany(Place::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRounds()
    {
        return $this->hasMany(Round::className(), ['href_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRounds0()
    {
        return $this->hasMany(Round::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShareDatas()
    {
        return $this->hasMany(ShareData::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Status::className(), ['user_id' => 'id']);
    }
}
