<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MobileUser;

/**
 * MobileUserSearch represents the model behind the search form about `app\models\MobileUser`.
 */
class MobileUserSearch extends MobileUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['login', 'token', 'push_token', 'last_activity_datetime', 'rate', 'rate_end_datetime', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MobileUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'last_activity_datetime' => $this->last_activity_datetime,
            'rate_end_datetime' => $this->rate_end_datetime,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere(['like', 'push_token', $this->push_token])
            ->andFilterWhere(['like', 'rate', $this->rate]);

        return $dataProvider;
    }
}
