<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $name Название
 * @property string $position1 Позиция1
 * @property string $position2 Позиция2
 * @property string $position3 Позиция3
 * @property string $position4 Позиция4
 * @property double $radius Радиус
 *
 * @property MobileUser $user
 * @property Report[] $reports
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['radius'], 'number'],
            [['name', 'position1', 'position2', 'position3', 'position4'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название',
            'position1' => 'Позиция1',
            'position2' => 'Позиция2',
            'position3' => 'Позиция3',
            'position4' => 'Позиция4',
            'radius' => 'Радиус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::className(), ['place_id' => 'id']);
    }
}
