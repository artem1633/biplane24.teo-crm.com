<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "round".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $href_user_id Пользователь на кого ссылаемся
 * @property string $href Ссылка
 * @property string $name ФИО
 * @property int $status_id Статус должность
 * @property int $group_id Группа
 *
 * @property Group $group
 * @property MobileUser $hrefUser
 * @property Status $status
 * @property MobileUser $user
 */
class Round extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'round';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status_id', 'group_id'], 'integer'],
            [['href', 'name'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['href_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['href_user_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['href_user_id', function($model){
                if(Yii::$app->module->id == 'api'){
                    $repeat = self::find()->where(['user_id' => Yii::$app->mobileUser->id, 'href_user_id' => $this->href_user_id])->one();

                    if($repeat){
                        $this->addError('href_user_id', 'Данная ссылка уже существует');
                        return false;
                    }
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'href_user_id' => 'Пользователь на кого ссылаемся',
            'href' => 'Ссылка',
            'name' => 'ФИО',
            'status_id' => 'Статус должность',
            'group_id' => 'Группа',
        ];
    }

    public function beforeSave($insert)
    {
        /** @var ShareData $href */
        $href = ShareData::find()->where(['href' => $this->href])->one();

        if($href != null){
            $this->href_user_id = $href->user_id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHrefUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'href_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'user_id']);
    }
}
