<?php

namespace app\models;

use app\helpers\Coordinates;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 */
class ReportSearch extends Report
{
    /**
     * @var integer
     */
    public $group_id;

    /**
     * @var integer
     */
    public $status_id;

    /**
     * @var integer
     */
    public $round_id;

    /**
     * @var string
     */
    public $datetime_start;

    /**
     * @var string
     */
    public $datetime_end;

    public $filterMe;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'value', 'place_id', 'group_id', 'status_id', 'round_id'], 'integer'],
            [['app_id', 'coords', 'datetime', 'datetime_start', 'datetime_end', 'filterMe'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $modelName = null)
    {
        $query = Report::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($modelName == null){
            $this->load($params);
        } else {
            $this->load($params, $modelName);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'value' => $this->value,
            'place_id' => $this->place_id,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'app_id', $this->app_id])
            ->andFilterWhere(['like', 'coords', $this->coords]);

        $query->andFilterWhere(['between', 'datetime', $this->datetime_start, $this->datetime_end]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchComplicated($params, $modelName, $userId)
    {
        $placeFilter = false;
        $selfFilter = false;
        $roundHrefs = [];
        $weekDays = [];
        $userPks = [];
        $positions = [];

        $this->load($params, $modelName);


        $this->place_id = explode(',', $this->place_id);

        if($this->place_id != null){
            $places = Place::find()->where(['id' => $this->place_id])->all();

            foreach($places as $place){
                list($x, $y) = explode(',', $place->position1);

                $positions[] = [$x, $y];

                $placeFilter = true;
                $selfFilter = true;
            }
        }


        if($this->group_id != null){
            // $group = Group::findOne($this->group_id);

            $rounds = Round::find()->andFilterWhere(['group_id' => $this->group_id])->all();

            $roundHrefs = ArrayHelper::merge($roundHrefs, ArrayHelper::getColumn($rounds, 'href'));

            // if($group != null){
            $pks = ArrayHelper::getColumn($rounds, 'href_user_id');

            $userPks = ArrayHelper::merge($userPks, $pks);
            $selfFilter = false;
            // }
        }

        if($this->status_id != null){
            // $status = Status::findOne($this->status_id);
            $rounds = Round::find()->andFilterWhere(['status_id' => $this->status_id])->all();

            $roundHrefs = ArrayHelper::merge($roundHrefs, ArrayHelper::getColumn($rounds, 'href'));

            // if($status != null){
            $pks = ArrayHelper::getColumn($rounds, 'href_user_id');

            $userPks = ArrayHelper::merge($userPks, $pks);
            $selfFilter = false;
            // }
        }

        $this->round_id = explode(',', $this->round_id);

        if($this->round_id != null)
        {
            $rounds = Round::find()->andFilterWhere(['id' => $this->round_id])->all();
            $pks = ArrayHelper::getColumn($rounds, 'href_user_id');

            $roundHrefs = ArrayHelper::merge($roundHrefs, ArrayHelper::getColumn($rounds, 'href'));

//            foreach ($rounds as $round){
//                $weekDays[] = explode(',', $round->week_days);
//            }

            //            $userPks = ArrayHelper::merge($userPks, $pks);
            $userPks = $pks;


            $selfFilter = false;
            // foreach($rounds as $round){
            // $userPks[] = $round->href_user_id;
            // }
        }

        $shareData = ShareData::find()->andFilterWhere(['href' => $roundHrefs])->all();

        foreach ($shareData as $data)
        {
            $weekDays = ArrayHelper::merge($weekDays, explode(',', $data->week_days));
        }

        $userPks = array_unique($userPks);

        if($this->datetime_start != null || $this->datetime_end != null){
            $this->datetime_start = strtotime($this->datetime_start);
            $this->datetime_end = strtotime($this->datetime_end);
        }

        Yii::warning($this->datetime_start.' '.$this->datetime_end);

        $models = Report::find()
            ->andFilterWhere(['between', 'time_start', $this->datetime_start, $this->datetime_end]);

        $weekDays = array_unique($weekDays);
        if(count($weekDays) > 0){
            $models->andWhere(['in', 'week_day', $weekDays]);
        }

        if(count($userPks) == 0){
            $userPks = ArrayHelper::getColumn(Round::find()->where(['user_id' => $userId])->all(), 'user_id');
            $userPks = ArrayHelper::merge($userPks, ArrayHelper::getColumn(Round::find()->where(['user_id' => $userId])->all(), 'href_user_id'));
        }

        // var_dump($userPks);
        // exit;

        if(count($userPks) == 0){
            $userPks = [$userId];
        }


        // if($placeFilter == false){
            $models->andFilterWhere(['user_id' => $userPks]);
        // }

        if($selfFilter == false){
            // $models->andWhere(['!=', 'user_id', $userId]);
        }

        if($this->filterMe == 1){
            $models->andWhere(['user_id' => $userId]);
        }

        $models = $models->all();

        if($placeFilter) {
            // Yii::info(ArrayHelper::getColumn($models, 'id'), 'before place distance filtering');
            $models = array_filter($models, function ($report) use ($positions, $place) {
                foreach ($positions as $position) {
                    if ($report->coords != null) {
                        list($x2, $y2) = explode(',', $report->coords);
                        $radius = Coordinates::getDistance($position[0], $position[1], $x2, $y2);

                        return $place->radius >= $radius;
                    }
                }
                return false;
            });
        }

//        $weekDays = array_unique($weekDays);
//        if(count($weekDays) > 0){
//            $models = array_filter($models, function($model) use ($weekDays) {
//                $day = intval(date('N', strtotime($model->time_start)));
//                return in_array($day, $weekDays);
//            });
//        }

        $dataProvider = new ArrayDataProvider();

        $dataProvider->setModels($models);

        return $dataProvider;
    }
}
