<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Round;

/**
 * RoundSearch represents the model behind the search form about `app\models\Round`.
 */
class RoundSearch extends Round
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'href_user_id', 'status_id', 'group_id'], 'integer'],
            [['href', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Round::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'href_user_id' => $this->href_user_id,
            'status_id' => $this->status_id,
            'group_id' => $this->group_id,
        ]);

        $query->andFilterWhere(['like', 'href', $this->href])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
