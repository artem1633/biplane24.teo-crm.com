<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "location".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $app_id ID приложения
 * @property string $coords Координаты
 * @property int $value Значение
 * @property int $place_id Место
 * @property string $datetime Дата и время
 * @property integer $time_start
 * @property integer $time_work
 * @property integer $week_day День недели
 *
 * @property Place $place
 * @property MobileUser $user
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            // [
                // 'class' => TimestampBehavior::class,
                // 'updatedAtAttribute' => null,
                // 'createdAtAttribute' => 'datetime',
                // 'value' => date('Y-m-d H:i:s')
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'value', 'place_id', 'time_start', 'time_work', 'week_day'], 'integer'],
            [['datetime'], 'safe'],
            [['app_id', 'coords'], 'string', 'max' => 255],
            [['place_id'], 'exist', 'skipOnError' => true, 'targetClass' => Place::className(), 'targetAttribute' => ['place_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'app_id' => 'ID приложения',
            'coords' => 'Координаты',
            'value' => 'Значение',
            'time_start' => 'Время начала',
            'time_work' => 'Время работы',
            'place_id' => 'Место',
            'datetime' => 'Дата и время',
            'week_day' => 'День недели'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->coords != null){
            $locations = self::find()->where(['user_id' => $this->user_id])->andWhere(['!=', 'id', $this->id])->andWhere(['coords' => null])->all();

            foreach ($locations as $location) {
                $location->coords = $this->coords;
                $location->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'user_id']);
    }
}
