<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "share_data".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $href Ссылка
 * @property string $time_from Время с
 * @property string $time_to Время по
 * @property string $comment Комментарий
 * @property string $week_days
 *
 * @property MobileUser $user
 */
class ShareData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'share_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time_from', 'time_to'], 'required'],
            [['user_id'], 'integer'],
            [['time_from', 'time_to'], 'safe'],
            [['comment'], 'string'],
            [['href', 'week_days'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MobileUser::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'href' => 'Ссылка',
            'time_from' => 'Время с',
            'time_to' => 'Время по',
            'comment' => 'Комментарий',
            'week_days' => 'Дни недели'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $length = 8;
            $chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

            do {
                $string ='';
                for( $i = 0; $i < $length; $i++) {
                    $string .= $chars[rand(0,strlen($chars)-1)];
                }
            } while ( ShareData::find()->where(['href' => $string])->one() != null );

            $this->href = 'http://biplane24.teo-crm.com/'.$string;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MobileUser::className(), ['id' => 'user_id']);
    }
}
