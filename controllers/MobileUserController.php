<?php

namespace app\controllers;

use app\models\GroupSearch;
use app\models\PlaceSearch;
use app\models\ReportSearch;
use app\models\RoundSearch;
use app\models\ShareDataSearch;
use app\models\StatusSearch;
use Yii;
use app\models\MobileUser;
use app\models\MobileUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * MobileUserController implements the CRUD actions for MobileUser model.
 */
class MobileUserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MobileUser models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new MobileUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single MobileUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $groupSearchModel = new GroupSearch();
        $groupDataProvider = $groupSearchModel->search(Yii::$app->request->queryParams);
        $groupDataProvider->query->andWhere(['user_id' => $model->id]);

        $placeSearchModel = new PlaceSearch();
        $placeDataProvider = $placeSearchModel->search(Yii::$app->request->queryParams);
        $placeDataProvider->query->andWhere(['user_id' => $model->id]);

        $roundSearchModel = new RoundSearch();
        $roundDataProvider = $roundSearchModel->search(Yii::$app->request->queryParams);
        $roundDataProvider->query->andWhere(['user_id' => $model->id]);

        $shareDataSearchModel = new ShareDataSearch();
        $shareDataDataProvider = $shareDataSearchModel->search(Yii::$app->request->queryParams);
        $shareDataDataProvider->query->andWhere(['user_id' => $model->id]);

        $statusSearchModel = new StatusSearch();
        $statusDataProvider = $statusSearchModel->search(Yii::$app->request->queryParams);
        $statusDataProvider->query->andWhere(['user_id' => $model->id]);

        $reportSearchModel = new ReportSearch();
        $reportDataProvider = $reportSearchModel->search(Yii::$app->request->queryParams);
        $reportDataProvider->query->andWhere(['user_id' => $model->id]);


        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                        'groupSearchModel' => $groupSearchModel,
                        'groupDataProvider' => $groupDataProvider,
                        'placeSearchModel' => $placeSearchModel,
                        'placeDataProvider' => $placeDataProvider,
                        'roundSearchModel' => $roundSearchModel,
                        'roundDataProvider' => $roundDataProvider,
                        'shareDataSearchModel' => $shareDataSearchModel,
                        'shareDataDataProvider' => $shareDataDataProvider,
                        'reportSearchModel' => $reportSearchModel,
                        'reportDataProvider' => $reportDataProvider,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $model,
                'groupSearchModel' => $groupSearchModel,
                'groupDataProvider' => $groupDataProvider,
                'placeSearchModel' => $placeSearchModel,
                'placeDataProvider' => $placeDataProvider,
                'roundSearchModel' => $roundSearchModel,
                'roundDataProvider' => $roundDataProvider,
                'shareDataSearchModel' => $shareDataSearchModel,
                'shareDataDataProvider' => $shareDataDataProvider,
                'reportSearchModel' => $reportSearchModel,
                'reportDataProvider' => $reportDataProvider,
            ]);
        }
    }

    /**
     * Creates a new MobileUser model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new MobileUser();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Добавить пользователь",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Добавить пользователь",
                    'content'=>'<span class="text-success">Создание пользователя успешно завершено</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Добавить пользователь",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing MobileUser model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить пользователь #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "пользователь #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Изменить пользователь #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing MobileUser model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing MobileUser model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the MobileUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MobileUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MobileUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
