<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Settings;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

//use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @throws ForbiddenHttpException
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->identity->isSuperAdmin() == false) {
            throw new ForbiddenHttpException();
        }

        $request = Yii::$app->request;

        $model = Yii::$app->user->identity;

        if ($request->isPost) {

            $data = $request->post();

            $model->load($request->post());
            $model->save();


            foreach ($data['Settings'] as $key => $value) {
                $setting = Settings::findByKey($key);

                if ($setting != null) {
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }
        }
        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
            'model' => $model,
        ]);
    }

    /**
     * @param string $token
     * @throws ForbiddenHttpException
     * @return array
     */
    public function actionConnectTelegram($token)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->user->identity->isSuperAdmin() == false) {
            throw new ForbiddenHttpException();
        }

        $proxy_server = Settings::findByKey('proxy_server')->value;

        $result = null;

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://illume.teo-crm.com/api/telegram-notify/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
        //        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $str1=strpos($result, "{");
        $str2=strpos($result, "}");
        $result=substr($result, $str1, $str2);
        $result = json_decode($result, true);

        return $result;
    }
}
