<?php

namespace app\modules\api\controllers;

use app\helpers\Coordinates;
use app\models\Place;
use app\models\Report;
use app\models\ReportSearch;
use Yii;
use app\models\MobileUser;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use app\models\Group;
use app\models\Round;
use app\models\Status;

class ReportController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;
        $searchModel = new ReportSearch();
        $data = ['model' => $request->get()];
        $dataProvider = $searchModel->search($data, 'model');
        // var_dump(Yii::$app->request->queryParams);
        // exit;
        $dataProvider->query->andWhere(['user_id' => $this->user->id]);
        $dataProvider->pagination = false;

        return $dataProvider->models;
    }


    /**
     * POST
     */
    public function actionSearch()
    {
        $request = Yii::$app->request;
        $searchModel = new ReportSearch();
        $data = ['model' => $request->get()];
        $dataProvider = $searchModel->searchComplicated($data, 'model', $this->user->id);
        // var_dump(Yii::$app->request->queryParams);
        // exit;
//        $dataProvider->query->andWhere();
        $dataProvider->pagination = false;

        return $dataProvider->models;
    }


    public function actionFindByPlace($id)
    {
        $place = Place::findOne($id);

        if($place == null){
            throw new NotFoundHttpException();
        }

        list($x, $y) = explode(',', $place->position1);

        /** @var Report[] $reports */
        $reports = Report::find()->where(['user_id' => $place->user_id])->all();

        $reports = array_filter($reports, function($report) use($x, $y, $place){
            if($report->coords != null){
                list($x2, $y2) = explode(',', $report->coords);
                $radius = Coordinates::getDistance($x, $y, $x2, $y2);

                return $place->radius >= $radius;
            }
            return false;
        });


        return $reports;
    }

    public function actionFindByGroup($id, $datetime_start = null, $datetime_end = null)
    {
        $group = Group::findOne($id);

        if($group == null){
            throw new NotFoundHttpException();
        }

        $usersPks = ArrayHelper::getColumn(Round::find()->where(['user_id' => $group->user_id])->all(), 'href_user_id');

        $reports = Report::find()->where(['user_id' => $usersPks])->andFilterWhere(['between', 'datetime', $datetime_start, $datetime_end])->all();

        return $reports;
    }

    public function actionFindByStatus($id, $datetime_start = null, $datetime_end = null)
    {
        $status = Status::findOne($id);

        if($status == null){
            throw new NotFoundHttpException();
        }

        $usersPks = ArrayHelper::getColumn(Round::find()->where(['user_id' => $status->user_id])->all(), 'href_user_id');

        $reports = Report::find()->where(['user_id' => $usersPks])->andFilterWhere(['between', 'datetime', $datetime_start, $datetime_end])->all();

        return $reports;
    }

    public function actionFindByRound($id, $datetime_start = null, $datetime_end = null)
    {
        $round = Round::findOne($id);

        if($round == null){
            throw new NotFoundHttpException();
        }

        $usersPks = $round->href_user_id;

        $reports = Report::find()->where(['user_id' => $usersPks])->andFilterWhere(['between', 'datetime', $datetime_start, $datetime_end])->all();

        return $reports;
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Report();


        if ($model->load($data, 'model') && $model->validate()) {
            $model->user_id = $this->user->id;
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->findModel($id);


        if ($model->load($data, 'model') && $model->validate()) {
            $model->user_id = $this->user->id;
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        return $model;
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        return ['result' => $model->delete()];
    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::find()->where(['id' => $id, 'user_id' => $this->user->id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }


    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }
}