<?php

namespace app\source\counters;

/**
 * Interface IComputer
 * @package app\source\counters
 * Интерфейс предназначен для реализации подсчета чего бы то нибыло
 */
interface IComputer
{
    /**
     * Выполняет подсчет
     * @return mixed
     */
    public function compute();
}