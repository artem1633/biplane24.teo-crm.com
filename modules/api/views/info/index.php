<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>API Документация</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        body {
            padding: 10px;
        }

        h3 {
            margin-top: 0;
            margin-bottom: 30px;
        }

        h3.no-margin-bottom {
            margin-top: 0;
            margin-bottom: 5px;
        }

        table {
            width: 1000px;
        }
    </style>
</head>
<body>

<h3>Сущности</h3>
<h4>Статус (status)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>name</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h4>Группа (group)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>name</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h4>Место (place)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>name</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное</td>
        <td></td>
    </tr>
    <tr>
        <td>position1</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное в формате x,y</td>
        <td>39.807972,-75.058007</td>
    </tr>
    <tr>
        <td>position2</td>
        <td><div class="label label-primary">String</div></td>
        <td>В формате x,y</td>
        <td>39.807972,-75.058007</td>
    </tr>
    <tr>
        <td>position3</td>
        <td><div class="label label-primary">String</div></td>
        <td>В формате x,y</td>
        <td>39.807972,-75.058007</td>
    </tr>
    <tr>
        <td>position4</td>
        <td><div class="label label-primary">String</div></td>
        <td>В формате x,y</td>
        <td>39.807972,-75.058007</td>
    </tr>
    <tr>
        <td>radius</td>
        <td><div class="label label-success">Integer</div></td>
        <td>В метрах</td>
        <td>20</td>
    </tr>
    </tbody>
</table>
<h4>Поделиться данными (share-data)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>time_from</td>
        <td><div class="label label-primary">String</div></td>
        <td>Время в формате (i:s)</td>
        <td>15:30</td>
    </tr>
    <tr>
        <td>time_to</td>
        <td><div class="label label-primary">String</div></td>
        <td>Время в формате (i:s)</td>
        <td>01:10</td>
    </tr>
    <tr>
        <td>comment</td>
        <td><div class="label label-primary">String</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>week_days</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дни недели через запятую в числовом формате</td>
        <td>1,2,3,5</td>
    </tr>
    </tbody>
</table>
<h4>Окружения (round)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <!--    <tr>-->
    <!--        <td>href_user_id</td>-->
    <!--        <td><div class="label label-danger">Integer (User)</div></td>-->
    <!--        <td>Обязательное</td>-->
    <!--        <td></td>-->
    <!--    </tr>-->
    <tr>
        <td>href</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное</td>
        <td></td>
    </tr>
    <tr>
        <td>name</td>
        <td><div class="label label-primary">String</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>status_id</td>
        <td><div class="label label-danger">Integer (Status)</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>group_id</td>
        <td><div class="label label-danger">Integer (Group)</div></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<h4>Отчет (report)</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Свойство</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>app_id</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное</td>
        <td></td>
    </tr>
    <tr>
        <td>coords</td>
        <td><div class="label label-primary">String</div></td>
        <td>В формате x,y</td>
        <td>39.807972,-75.058007</td>
    </tr>
    <tr>
        <td>value</td>
        <td><div class="label label-success">Integer</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>time_start</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Милисекунды</td>
        <td></td>
    </tr>
    <tr>
        <td>time_work</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Милисекунды</td>
        <td></td>
    </tr>
    <tr>
        <td>place_id</td>
        <td><div class="label label-danger">Integer (Place)</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>group_id</td>
        <td><div class="label label-danger">Integer (Group)</div></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>datetime</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s)</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>week_day</td>
        <td><div class="label label-success">Integer</div></td>
        <td>День недели в числовом формате</td>
        <td>1</td>
    </tr>
    <tr>
        <td>filterMe</td>
        <td><div class="label label-success">Integer</div></td>
        <td>1 или 0 (По умолчанию 0)</td>
        <td>1</td>
    </tr>
    </tbody>
</table>

<h3 style="margin-top: 40px; margin-bottom: 5px;">Методы</h3>
<p style="margin-bottom: 50px;">Отправка происходит в виде передачи параметров в GET и в POST запросе если это необхадимо. Ответ приходит в формате JSON</p>

<h3 class="no-margin-bottom">Регистрация пользователя</h3>
<a href="api/user/register">api/user/register</a><br>
<h4 class="text text-danger">POST</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>login</td>
        <td><div class="label label-primary">String</div></td>
        <td>Обязательное в виде номера телефона</td>
        <td>7999888</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает</b></h5>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Тип</th>
        <th scope="col">Описание</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>token</td>
        <td><div class="label label-primary">String</div></td>
        <td>Токен для авторизации</td>
        <td>K0i5vTBTKCgQemz7S22x5lrUXpUy23le</td>
    </tr>
    </tbody>
</table>
<hr>

<h2 class="text-center">Методы для работы с отчетами</h2>

<hr>

<h3 class="no-margin-bottom">Поиск отчетов по месту</h3>
<p>Метод требует авторизации</p>
<a href="api/report/find-by-place">api/report/find-by-place</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Обязательное поле | ID места</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>

<h3 class="no-margin-bottom">Поиск отчетов</h3>
<p>Метод требует авторизации</p>
<a href="api/report/search">api/report/search</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>group_id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID группы</td>
        <td></td>
    </tr>
    <tr>
        <td>status_id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID статуса</td>
        <td></td>
    </tr>
    <tr>
        <td>place_id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID места</td>
        <td></td>
    </tr>
    <tr>
        <td>round_id</td>
        <td><div class="label label-primary">String</div></td>
        <td>ID окружений через запятую</td>
        <td>23,67,12,96,12</td>
    </tr>
    <tr>
        <td>datetime_start</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты начиная с этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>datetime_end</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты заканчивая этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>

<h3 class="no-margin-bottom">Поиск отчетов по группе</h3>
<p>Метод требует авторизации</p>
<a href="api/report/find-by-group">api/report/find-by-group</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Обязательное поле | ID группы</td>
        <td></td>
    </tr>
    <tr>
        <td>datetime_start</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты начиная с этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>datetime_end</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты заканчивая этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>

<h3 class="no-margin-bottom">Поиск отчетов по статусу</h3>
<p>Метод требует авторизации</p>
<a href="api/report/find-by-status">api/report/find-by-status</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Обязательное поле | ID статуса</td>
        <td></td>
    </tr>
    <tr>
        <td>datetime_start</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты начиная с этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>datetime_end</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты заканчивая этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>

<h3 class="no-margin-bottom">Поиск отчетов по окружению</h3>
<p>Метод требует авторизации</p>
<a href="api/report/find-by-round">api/report/find-by-round</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Обязательное поле | ID окружения</td>
        <td></td>
    </tr>
    <tr>
        <td>datetime_start</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты начиная с этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>datetime_end</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты заканчивая этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>

<h3 class="no-margin-bottom">Получить мои отчеты</h3>
<p>Метод требует авторизации</p>
<a href="api/report/index">api/report/index</a><br>
<h4 class="text text-danger">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>datetime_start</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты начиная с этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    <tr>
        <td>datetime_end</td>
        <td><div class="label label-primary">String</div></td>
        <td>Дата и время в формате (Y-m-d H:i:s) | Фильтрует отчеты заканчивая этой даты</td>
        <td>2020-01-01 00:00:00</td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает:</b> массив сущностей Report</h5>
<hr>
<hr>
<br>
<br>
<br>
<br>

<h3 class="no-margin-bottom">Установить FCM-токен</h3>
<p>Метод требует авторизации</p>
<a href="api/user/set-fcm-token">api/user/set-fcm-token</a>
<h4 class="text text-danger">POST</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>fmc_token</td>
        <td><div class="label label-primary">String</div></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает</b></h5>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Тип</th>
        <th scope="col">Описание</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>result</td>
        <td><div class="label label-warning">Boolean</div></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<hr>

<h3 class="no-margin-bottom">Отправить SMS</h3>
<a href="https://sms.ru/sms/send">https://sms.ru/sms/send</a>
<h4 class="text text-primary">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>api_id</td>
        <td><div class="label label-primary">String</div></td>
        <td>Токен который получен с помощью соответствующего метода</td>
        <td></td>
    </tr>
    <tr>
        <td>to</td>
        <td><div class="label label-primary">String</div></td>
        <td>Один или несколько номеров, указанных через запятую</td>
        <td></td>
    </tr>
    <tr>
        <td>msg</td>
        <td><div class="label label-primary">String</div></td>
        <td>Сообщение</td>
        <td></td>
    </tr>
    <tr>
        <td>json</td>
        <td><div class="label label-success">Integer</div></td>
        <td>Всегда должен быть <b>1</b></td>
        <td></td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает</b></h5>
См. пример ответа <a href="http://sms.ru/api/send" target="_blank">тут</a>
<hr>

<h3 class="no-margin-bottom">Получить SMS токен</h3>
<a href="api/user/get-sms-token">api/user/get-sms-token</a>
<h4 class="text text-primary">GET</h4>
<h5><b>Отдает</b></h5>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Тип</th>
        <th scope="col">Описание</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>token</td>
        <td><div class="label label-primary">String</div></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<hr>


<h3 style="margin-top: 40px; margin-bottom: 5px;">CRUD-Методы</h3>
<p style="margin-bottom: 5px;">CRUD доступен для сущностей описанный в разделе «Сущности». URL запроса строится следующем способом /api/{object}/{method}</p>
<p style="margin-bottom: 5px;">{object} — сущность</p>
<p style="margin-bottom: 5px;">{method} — метод. Возможные методы: <b>INDEX</b>, <b>CREATE</b>, <b>UPDATE</b>, <b>VIEW</b>, <b>DELETE</b></p>
<p style="margin-bottom: 5px;">Пример URL запроса: api/place/update?id=2</p>
<p style="margin-bottom: 50px;">Все CRUD методы требует авторизации путем передачи параметра <b>token</b></p>

<h3 class="no-margin-bottom">Index</h3>
<a href="api/user/set-fcm-token">api/{object}/index</a>
<h4 class="text text-primary">GET</h4>
<h5><b>Отдает</b></h5>
<p>Массив объектов. Которые принадлежат данному пользователю</p>
<hr>

<h3 class="no-margin-bottom">Create</h3>
<a href="api/user/set-fcm-token">api/{object}/create</a>
<h4 class="text text-danger">POST</h4>
<p>Объект</p>
<h5><b>Отдает</b></h5>
<p>Объект</p>
<hr>

<h3 class="no-margin-bottom">Update</h3>
<a href="api/user/set-fcm-token">api/{object}/update</a>
<h4 class="text text-primary">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID объекта</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h4 class="text text-danger">POST</h4>
<p>Объект</p>
<h5><b>Отдает</b></h5>
<p>Объект</p>
<hr>

<h3 class="no-margin-bottom">View</h3>
<a href="api/user/set-fcm-token">api/{object}/view</a>
<h4 class="text text-primary">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID объекта</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h5><b>Отдает</b></h5>
<p>Объект</p>
<hr>

<h3 class="no-margin-bottom">DELETE</h3>
<a href="api/user/set-fcm-token">api/{object}/delete</a>
<h4 class="text text-primary">GET</h4>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Параметр</th>
        <th scope="col">Тип</th>
        <th scope="col">Валидация</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>id</td>
        <td><div class="label label-success">Integer</div></td>
        <td>ID объекта</td>
        <td></td>
    </tr>
    </tbody>
</table>
<h4 class="text text-danger">POST</h4>
<p><i>Пусто</i></p>
<h5><b>Отдает</b></h5>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th scope="col">Поле</th>
        <th scope="col">Тип</th>
        <th scope="col">Описание</th>
        <th scope="col">Пример</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>result</td>
        <td><div class="label label-warning">Boolean</div></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<p>Объект</p>
<hr>

</body>
</html>