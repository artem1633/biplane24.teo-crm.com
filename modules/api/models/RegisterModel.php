<?php

namespace app\modules\api\models;

use app\models\MobileUser;
use app\models\MUserWallet;
use Yii;
use yii\base\Model;

/**
 * Class RegisterModel
 * @package app\modules\api\models
 */
class RegisterModel extends Model
{
    /**
     * @var string
     */
    public $login;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login'], 'string'],
            ['login', 'unique', 'targetClass' => MobileUser::className(), 'targetAttribute' => 'login'],
        ];
    }

    /**
     * @return string
     */
    public function register()
    {
        if($this->validate()){
            $user = new MobileUser([
                'login' => $this->login,
            ]);
            $user->save(false);

            return $user;
        }

        return null;
    }
}