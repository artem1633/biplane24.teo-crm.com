<?php

namespace app\modules\api;

use Yii;
use app\components\MobileUser;
use yii\web\Response;
use app\models\MobileUser as User;

/**
 * api module definition class
 */
class Api extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();


        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $user = User::find()->where(['token' => $token])->one();
        } else {
            $user = null;
        }

        Yii::$container->setDefinitions([
            'app\components\MobileUser' => [
                'class' => MobileUser::class,
                'identity' => $user,
            ],
        ]);
    }
}
