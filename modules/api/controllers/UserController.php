<?php

namespace app\modules\api\controllers;

use app\models\Settings;
use app\models\User;
use Yii;
use app\models\MobileUser;
use app\models\PasswordForget;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new RegisterModel();

        if($model->load($data, 'model')) {

            $user = MobileUser::find()->where(['login' => $model->login])->one();

            if($user != null){
                return ['result' => $user->token, 'id' => $user->id];
            }

            $user = $model->register();

            if($user == null){
                return $model->errors;
            }

            return ['result' => $user->token, 'id' => $user->id];
        }

        return ['result' => false];
    }

    /**
     * POST
     */
    public function actionSetFcmToken()
    {
        $token = $_POST['fcm_token'];

        $this->user->push_token = $token;
        $result = $this->user->save(false);

        return ['result' => $result];
    }

    public function actionGetSmsToken()
    {
        $token = Settings::findByKey('sms_token')->value;

        return ['token' => $token];
    }

//    /**
//     * POST
//     */
//    public function actionGetToken()
//    {
//        $request = Yii::$app->request;
//        $data = ['model' => $request->post()];
//        $model = new Login();
//
//        if($model->load($data, 'model')) {
//
//            $token = $model->login();
//
//            if($token == null){
//                return $model->errors;
//            }
//
//            return ['result' => $token];
//        }
//
//        return ['result' => false];
//    }

    public function beforeAction($action)
    {
        if(in_array($action->id, ['change-password', 'set-fcm-token'])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = MobileUser::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }
        }

        return parent::beforeAction($action);
    }
}