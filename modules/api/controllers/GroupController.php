<?php

namespace app\modules\api\controllers;

use app\models\Group;
use app\models\GroupSearch;
use Yii;
use app\models\MobileUser;
use app\modules\api\models\Login;
use app\modules\api\models\RegisterModel;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class GroupController extends Controller
{
    /**
     * @var MobileUser
     */
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * POST
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => $this->user->id]);

        return $dataProvider->models;
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = new Group();


        if ($model->load($data, 'model') && $model->validate()) {
            $model->user_id = $this->user->id;
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $data = ['model' => $request->post()];
        $model = $this->findModel($id);


        if ($model->load($data, 'model') && $model->validate()) {
        	$model->user_id = $this->user->id;
            $model->save(false);
            return $model;
        } else {
            return $model->errors;
        }
    }

    public function actionView($id)
    {
    	$request = Yii::$app->request;
        $model = $this->findModel($id);
        return $model;
    }

    public function actionDelete($id)
    {
    	$request = Yii::$app->request;
        $model = $this->findModel($id);
        return ['result' => $model->delete()];
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::find()->where(['id' => $id, 'user_id' => $this->user->id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }


    public function beforeAction($action)
    {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $request = Yii::$app->request;
            $token = null;

            if($request->isPost){
                $token = isset($_POST['token']) ? $_POST['token'] : null;
            } else if ($request->isGet){
                $token = isset($_GET['token']) ? $_GET['token'] : null;
            }

            if($token){
                $this->user = MobileUser::find()->where(['token' => $token])->one();
            }

            if($this->user == null){
                throw new ForbiddenHttpException();
            }

        return parent::beforeAction($action);
    }
}