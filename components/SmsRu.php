<?php

namespace app\components;

use app\models\Settings;
use yii\base\Component;

/**
 * Class SmsRu
 * @package app\components
 */
class SmsRu extends Component
{
    private $token;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->token = Settings::findByKey('sms_token')->value;
    }

    /**
     * @return int
     */
    public function getBalance()
    {
        $response = json_decode(file_get_contents("https://sms.ru/my/balance?api_id={$this->token}&json=1"), 'true');

        if(isset($response['balance'])){
            return $response['balance'];
        }

        return 0;
    }
}