<?php

namespace app\components;

use yii\base\Component;

/**
 * Class MobileUser
 * @package app\components
 *
 * @property int $id
 */
class MobileUser extends Component
{
    /**
     * @var \app\models\MobileUser
     */
    public $identity;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->identity->id;
    }
}